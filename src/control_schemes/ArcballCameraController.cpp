//
// Created by anton on 13.06.22.
//

#include <iostream>
#include "ArcballCameraController.h"

#include "OgreApplicationContext.h"

using namespace Ogre;

bool ArcballCameraController::keyPressed(const OIS::KeyEvent &evt)
{
    if (evt.key == OIS::KC_LSHIFT) {
        m_shiftPressed = true;
    }
    return true;
}

bool ArcballCameraController::keyReleased(const OIS::KeyEvent &evt)
{
    if (evt.key == OIS::KC_LSHIFT) {
        m_shiftPressed = false;
    }
    return true;
}

bool ArcballCameraController::mouseMoved(const OIS::MouseEvent &evt)
{
    const auto scroll = static_cast<float>(evt.state.Z.rel);
    if (std::abs(scroll) > 1.0e-10) {
        m_cameraTransform.Zoom(scroll / 120);
        UpdateViewNode();
    }

    if (m_arcballMode != DISABLED) {
        const Vector2 position {static_cast<float>(evt.state.X.abs),
                                static_cast<float>(evt.state.Y.abs)};
        if (m_arcballMode == ROTATION) {
            m_cameraTransform.Rotate(position);
        } else {
            m_cameraTransform.Translate(position, 0.5);
        }
        UpdateViewNode();
    }

    return true;
}

bool ArcballCameraController::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if (id == OIS::MouseButtonID::MB_Left) {
        m_arcballMode = m_shiftPressed ? TRANSLATION : ROTATION;
        std::cout << "started" << std::endl;
        m_cameraTransform.InitTransformation({static_cast<float>(evt.state.X.abs),
                                              static_cast<float>(evt.state.Y.abs)});
    }
    return true;
}

bool ArcballCameraController::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    if (id == OIS::MouseButtonID::MB_Left) {
        m_arcballMode = DISABLED;
    }
    return true;
}

void ArcballCameraController::SetWindowSize(const Vector2 &windowSize)
{
    m_cameraTransform.Reshape(windowSize);
}

void ArcballCameraController::SetViewParameters(const Ogre::Vector3 &position,
                                                const Ogre::Vector3 &targetPoint,
                                                const Ogre::Vector3 &up) {
    m_cameraTransform.SetViewParameters(position, targetPoint, up);
    UpdateViewNode();
}

void ArcballCameraController::UpdateViewNode()
{
    Vector3 position, scale;
    Quaternion orientation;
    m_cameraTransform.transformation().decomposition(position, scale, orientation);

    std::cout << position << " or:" << orientation << std::endl;

    m_viewerNode->setPosition(position);
    m_viewerNode->setScale(scale);
    m_viewerNode->setOrientation(orientation);
}
