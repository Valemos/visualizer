#include "ArcballTransformation.h"

#include <iostream>

using namespace Ogre;

/* Project a point in NDC onto the arcball sphere */
Quaternion ndcToArcBall(const Vector2& p) {
    const float dist = p.dotProduct(p);

    /* Point is on sphere */
    if(dist <= 1.0f) {
        return {0.f, p.x, p.y, std::sqrt(1.0f - dist)};
    }
    /* Point is outside sphere */
    else {
        const Vector2 proj = p.normalisedCopy();
        return {0.0f, proj.x, proj.y, 0.0f};
    }
}

ArcballTransformation::ArcballTransformation(const Vector3& cameraPosition, const Vector3& viewCenter,
                                             const Vector3& upDir, const Degree &fov, const Vector2 &windowSize):
        m_fov{fov}, m_windowSize{windowSize}
{
    SetViewParameters(cameraPosition, viewCenter, upDir);
}

void ArcballTransformation::SetViewParameters(const Vector3& eye, const Vector3& viewCenter,
                                              const Vector3& upDir)
{
    const Vector3 dir = viewCenter - eye;
    Vector3 zAxis = dir.normalisedCopy();
    Vector3 xAxis = (zAxis.crossProduct(upDir.normalisedCopy())).normalisedCopy();
    Vector3 yAxis = (xAxis.crossProduct(zAxis)).normalisedCopy();
    xAxis = (zAxis.crossProduct(yAxis)).normalisedCopy();

    m_targetPosition = -viewCenter;
    m_zooming = -dir.length();

    Matrix3 rotationMtx;
    rotationMtx.FromAxes(xAxis, yAxis, -zAxis);
    m_targetRotation = Quaternion(rotationMtx.transpose());
    m_targetRotation.normalise();
    UpdateViewTransform();
}

void ArcballTransformation::InitTransformation(const Vector2& mousePos) {
    m_prevMousePosNDC = screenCoordToNDC(mousePos);
    m_prevRotation = ndcToArcBall(m_prevMousePosNDC);
}

void ArcballTransformation::Rotate(const Vector2& mousePos) {
    const Vector2 mousePosNDC = screenCoordToNDC(mousePos);
    const Quaternion currentQRotation = ndcToArcBall(mousePosNDC);
    m_prevRotation = ndcToArcBall(m_prevMousePosNDC);

    m_prevMousePosNDC = mousePosNDC;
    m_targetRotation = currentQRotation * m_prevRotation * m_targetRotation;
    m_targetRotation.normalise();

    UpdateViewTransform();
}

void ArcballTransformation::Translate(const Vector2 &mousePos, float translateFraction)
{
    const Vector2 mousePosNDC = screenCoordToNDC(mousePos);
    const Vector2 translationNDC = mousePosNDC - m_prevMousePosNDC;
    if (translationNDC.dotProduct(translationNDC) < 1.0e-10) {
        return;
    }

    m_prevMousePosNDC = mousePosNDC;
    TranslateDelta(translationNDC, translateFraction);
}

void ArcballTransformation::TranslateDelta(const Vector2& translationNDC, const float fraction) {
    /* Half size of the screen viewport at the view center and perpendicular
       with the viewDir */
    const float hh = std::abs(m_zooming) * std::tan(m_fov.valueRadians() * 0.5f);
    const float hw = hh * m_windowSize.x / m_windowSize.y;

    Vector3 delta{translationNDC.x * hw, translationNDC.y * hh, 0.0f};

    m_targetPosition += m_targetRotation * delta * fraction;
    UpdateViewTransform();
}

void ArcballTransformation::Zoom(float delta) {
    m_zooming += delta;
    UpdateViewTransform();
}

void ArcballTransformation::UpdateViewTransform() {
    m_view = Affine3(Vector3{0, 0, m_zooming}, Quaternion::IDENTITY) *
            Affine3(Vector3::ZERO, m_targetRotation) *
            Affine3(m_targetPosition, Quaternion::IDENTITY);
    m_inverseView = m_view.inverse();
}

Vector2 ArcballTransformation::screenCoordToNDC(const Vector2 &mousePos) const {
    return {mousePos.x * 2.0f / m_windowSize.x - 1.0f,
            1.0f - 2.0f * mousePos.y / m_windowSize.y};
}
