//
// Created by anton on 20.06.22.
//

#include "ArrowCameraController.h"

bool ArrowCameraController::keyPressed(const OIS::KeyEvent &arg) {
    return false;
}

bool ArrowCameraController::keyReleased(const OIS::KeyEvent &arg) {
    return false;
}

bool ArrowCameraController::mouseMoved(const OIS::MouseEvent &arg) {
    return false;
}

bool ArrowCameraController::mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id) {
    return false;
}

bool ArrowCameraController::mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id) {
    return false;
}

void ArrowCameraController::SetWindowSize(const Ogre::Vector2 &windowSize)
{

}

void ArrowCameraController::SetViewParameters(const Ogre::Vector3 &position, const Ogre::Vector3 &targetPoint,
                                              const Ogre::Vector3 &up)
{

}
