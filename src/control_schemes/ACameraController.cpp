//
// Created by anton on 19.06.22.
//

#include "OgreApplicationContext.h"
#include "ArcballCameraController.h"
#include <iostream>
#include "ACameraController.h"

ACameraController::ACameraController(Ogre::SceneManager *scene) {
    m_viewerNode = scene->getRootSceneNode()->createChildSceneNode();
    m_camera = scene->createCamera("viewerCam");
    m_camera->setNearClipDistance(5); // specific to this sample
    m_camera->setAutoAspectRatio(true);
    m_viewerNode->attachObject(m_camera);
}

ACameraController::ACameraController(Ogre::Camera *camera, Ogre::SceneNode *node) :
    m_camera(camera), m_viewerNode(node)
{
}

Ogre::Camera *ACameraController::GetCamera() {
    return m_camera;
}
