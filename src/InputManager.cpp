#include <sstream>
#include "InputManager.h"

InputManager *InputManager::m_instanceSingleton;

InputManager::InputManager() :
        m_mouse(nullptr ),
        m_keyboard(nullptr ),
        m_inputSystem(nullptr ) {
}

InputManager::~InputManager() {
    if( m_inputSystem ) {
        if( m_mouse ) {
            m_inputSystem->destroyInputObject(m_mouse );
            m_mouse = nullptr;
        }

        if( m_keyboard ) {
            m_inputSystem->destroyInputObject(m_keyboard );
            m_keyboard = nullptr;
        }

        OIS::InputManager::destroyInputSystem(m_inputSystem );
        m_inputSystem = nullptr;

        // Clear Listeners
        m_keyListeners.clear();
        m_mouseListeners.clear();
    }
}

void InputManager::initialise( Ogre::RenderWindow *renderWindow ) {
    if( !m_inputSystem ) {
        // Setup basic variables
        OIS::ParamList paramList;
        size_t windowHnd = 0;
        std::ostringstream windowHndStr;

        // Get window handle
        renderWindow->getCustomAttribute( "WINDOW", &windowHnd );

        // Fill parameter list
        windowHndStr << (unsigned int) windowHnd;
        paramList.insert( std::make_pair( std::string( "WINDOW" ), windowHndStr.str() ) );

        // linux grab parameters
        paramList.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
        paramList.insert(std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
        paramList.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
        paramList.insert(std::make_pair(std::string("XAutoRepeatOn"), std::string("true")));

        // Create inputsystem
        m_inputSystem = OIS::InputManager::createInputSystem(paramList );

        // If possible create a buffered keyboard
        // (note: if below line doesn't compile, try:  if (m_inputSystem->getNumberOfDevices(OIS::OISKeyboard) > 0) {
        //if( m_inputSystem->numKeyboards() > 0 ) {
        if (m_inputSystem->getNumberOfDevices(OIS::OISKeyboard) > 0) {
            m_keyboard = dynamic_cast<OIS::Keyboard*>( m_inputSystem->createInputObject(OIS::OISKeyboard, true ) );
            m_keyboard->setEventCallback(this );
        }

        // If possible create a buffered mouse
        // (note: if below line doesn't compile, try:  if (m_inputSystem->getNumberOfDevices(OIS::OISMouse) > 0) {
        //if( m_inputSystem->numMice() > 0 ) {
        if (m_inputSystem->getNumberOfDevices(OIS::OISMouse) > 0) {
            m_mouse = dynamic_cast<OIS::Mouse*>( m_inputSystem->createInputObject(OIS::OISMouse, true ) );
            m_mouse->setEventCallback(this );

            // Get window size
            unsigned int width, height, depth;
            int left, top;
            renderWindow->getMetrics( width, height, left, top );

            // Set mouse region
            this->setWindowExtents(static_cast<int>(width), static_cast<int>(height));
        }
    }
}

void InputManager::capture() {
    // Need to capture / update each device every frame
    if(m_mouse) {
        m_mouse->capture();
    }

    if(m_keyboard) {
        m_keyboard->capture();
    }
}

void InputManager::addKeyListener( OIS::KeyListener *keyListener, const std::string& instanceName ) {
    if( m_keyboard ) {
        // Check for duplicate items
        m_itKeyListener = m_keyListeners.find(instanceName );
        if(m_itKeyListener == m_keyListeners.end() ) {
            m_keyListeners[ instanceName ] = keyListener;
        }
        else {
            // Duplicate Item
        }
    }
}

void InputManager::addMouseListener( OIS::MouseListener *mouseListener, const std::string& instanceName ) {
    if( m_mouse ) {
        // Check for duplicate items
        m_itMouseListener = m_mouseListeners.find(instanceName );
        if(m_itMouseListener == m_mouseListeners.end() ) {
            m_mouseListeners[ instanceName ] = mouseListener;
        }
        else {
            // Duplicate Item
        }
    }
}

void InputManager::removeKeyListener( const std::string& instanceName ) {
    // Check if item exists
    m_itKeyListener = m_keyListeners.find(instanceName );
    if(m_itKeyListener != m_keyListeners.end() ) {
        m_keyListeners.erase(m_itKeyListener );
    }
    else {
        // Doesn't Exist
    }
}

void InputManager::removeMouseListener( const std::string& instanceName ) {
    // Check if item exists
    m_itMouseListener = m_mouseListeners.find(instanceName );
    if(m_itMouseListener != m_mouseListeners.end() ) {
        m_mouseListeners.erase(m_itMouseListener );
    }
    else {
        // Doesn't Exist
    }
}

void InputManager::removeKeyListener( OIS::KeyListener *keyListener ) {
    m_itKeyListener    = m_keyListeners.begin();
    m_itKeyListenerEnd = m_keyListeners.end();
    for(; m_itKeyListener != m_itKeyListenerEnd; ++m_itKeyListener ) {
        if(m_itKeyListener->second == keyListener ) {
            m_keyListeners.erase(m_itKeyListener );
            break;
        }
    }
}

void InputManager::removeMouseListener( OIS::MouseListener *mouseListener ) {
    m_itMouseListener    = m_mouseListeners.begin();
    m_itMouseListenerEnd = m_mouseListeners.end();
    for(; m_itMouseListener != m_itMouseListenerEnd; ++m_itMouseListener ) {
        if(m_itMouseListener->second == mouseListener ) {
            m_mouseListeners.erase(m_itMouseListener );
            break;
        }
    }
}

void InputManager::removeAllListeners() {
    m_keyListeners.clear();
    m_mouseListeners.clear();
}

void InputManager::removeAllKeyListeners() {
    m_keyListeners.clear();
}

void InputManager::removeAllMouseListeners() {
    m_mouseListeners.clear();
}

void InputManager::setWindowExtents( int width, int height ) {
    // Set mouse region (if window resizes, we should alter this to reflect as well)
    const OIS::MouseState &mouseState = m_mouse->getMouseState();
    mouseState.width  = width;
    mouseState.height = height;
}

OIS::Mouse* InputManager::getMouse() {
    return m_mouse;
}

OIS::Keyboard* InputManager::getKeyboard() {
    return m_keyboard;
}

bool InputManager::keyPressed( const OIS::KeyEvent &e ) {
    m_itKeyListener    = m_keyListeners.begin();
    m_itKeyListenerEnd = m_keyListeners.end();
    for(; m_itKeyListener != m_itKeyListenerEnd; ++m_itKeyListener ) {
        if(!m_itKeyListener->second->keyPressed(e ))
            break;
    }
    return true;
}

bool InputManager::keyReleased( const OIS::KeyEvent &e ) {
    m_itKeyListener    = m_keyListeners.begin();
    m_itKeyListenerEnd = m_keyListeners.end();
    for(; m_itKeyListener != m_itKeyListenerEnd; ++m_itKeyListener ) {
        if(!m_itKeyListener->second->keyReleased(e ))
            break;
    }
    return true;
}

bool InputManager::mouseMoved( const OIS::MouseEvent &e ) {
    m_itMouseListener    = m_mouseListeners.begin();
    m_itMouseListenerEnd = m_mouseListeners.end();
    for(; m_itMouseListener != m_itMouseListenerEnd; ++m_itMouseListener ) {
        if(!m_itMouseListener->second->mouseMoved(e ))
            break;
    }
    return true;
}

bool InputManager::mousePressed( const OIS::MouseEvent &e, OIS::MouseButtonID id ) {
    m_itMouseListener    = m_mouseListeners.begin();
    m_itMouseListenerEnd = m_mouseListeners.end();
    for(; m_itMouseListener != m_itMouseListenerEnd; ++m_itMouseListener ) {
        if(!m_itMouseListener->second->mousePressed(e, id ))
            break;
    }
    return true;
}

bool InputManager::mouseReleased( const OIS::MouseEvent &e, OIS::MouseButtonID id ) {
    m_itMouseListener    = m_mouseListeners.begin();
    m_itMouseListenerEnd = m_mouseListeners.end();
    for(; m_itMouseListener != m_itMouseListenerEnd; ++m_itMouseListener ) {
        if(!m_itMouseListener->second->mouseReleased(e, id ))
            break;
    }
    return true;
}

InputManager* InputManager::getSingletonPtr() {
    if( !m_instanceSingleton ) {
        m_instanceSingleton = new InputManager();
    }

    return m_instanceSingleton;
}
