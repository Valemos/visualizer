#include "CameraControllerCombination.h"
#include "ArcballCameraController.h"
#include "ArrowCameraController.h"

CameraControllerCombination::CameraControllerCombination(Ogre::SceneManager *scene, ControllerType type) : ACameraController(scene) {
    SetController(type);
}

bool CameraControllerCombination::keyPressed(const OIS::KeyEvent &arg)
{
    return m_controller->keyPressed(arg);
}

bool CameraControllerCombination::keyReleased(const OIS::KeyEvent &arg)
{
    return m_controller->keyReleased(arg);
}

bool CameraControllerCombination::mouseMoved(const OIS::MouseEvent &arg)
{
    return m_controller->mouseMoved(arg);
}

bool CameraControllerCombination::mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
    return m_controller->mousePressed(arg, id);
}

bool CameraControllerCombination::mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id)
{
    return m_controller->mouseReleased(arg, id);
}

void CameraControllerCombination::SetWindowSize(const Ogre::Vector2 &windowSize)
{
    m_controller->SetWindowSize(windowSize);
}

void CameraControllerCombination::SetViewParameters(const Ogre::Vector3 &position, const Ogre::Vector3 &targetPoint,
                                                    const Ogre::Vector3 &up)
{
    m_controller->SetViewParameters(position, targetPoint, up);
}

void CameraControllerCombination::SetController(ControllerType type)
{
    if (type == ARCBALL){
        m_controller = std::make_shared<ArcballCameraController>(m_camera, m_viewerNode);
    } else if (type == ARROWS) {
        m_controller = std::make_shared<ArrowCameraController>(m_camera, m_viewerNode);
    }
}
