#pragma once

#include "ACameraController.h"


enum ControllerType {
    ARCBALL,
    ARROWS
};

class CameraControllerCombination : public ACameraController
{
public:
    explicit CameraControllerCombination(Ogre::SceneManager *scene, ControllerType type);

    bool keyPressed(const OIS::KeyEvent &arg) override;
    bool keyReleased(const OIS::KeyEvent &arg) override;

    bool mouseMoved(const OIS::MouseEvent &arg) override;
    bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id) override;
    bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id) override;

    void SetWindowSize(const Ogre::Vector2 &windowSize) override;
    void SetViewParameters(const Ogre::Vector3 &position, const Ogre::Vector3 &targetPoint,
                           const Ogre::Vector3 &up) override;

    void SetController(ControllerType type);

private:
    ACameraController::Ptr m_controller;
};
