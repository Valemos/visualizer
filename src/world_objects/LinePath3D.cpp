//
// Created by anton on 19.06.22.
//

#include "LinePath3D.h"
#include <memory>

LinePath3D::LinePath3D(Ogre::SceneManager* sceneManager)
{
    m_object = sceneManager->createManualObject();
    m_node = sceneManager->getRootSceneNode()->createChildSceneNode();
    m_node->attachObject(m_object);

    m_material = CreateMaterial();
}

Ogre::MaterialPtr LinePath3D::CreateMaterial()
{
    auto material = std::static_pointer_cast<Ogre::Material>(Ogre::MaterialManager::getSingleton()
            .createOrRetrieve("LinePointerMaterial", "General").first);
    material->setReceiveShadows(false);
    material->getTechnique(0)->setLightingEnabled(true);
    material->getTechnique(0)->getPass(0)->setDiffuse(0, 0, 1, 0);
    material->getTechnique(0)->getPass(0)->setAmbient(0, 0, 1);
    material->getTechnique(0)->getPass(0)->setSelfIllumination(0, 0, 1);
    return material;
}

void LinePath3D::Recreate(const std::vector<Ogre::Vector3> &points)
{
    m_object->clear();
    m_object->begin(m_material, Ogre::RenderOperation::OT_LINE_LIST);
    for (const auto& p : points) {
        m_object->position(p);
    }
    m_object->end();
}

