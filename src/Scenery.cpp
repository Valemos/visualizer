#include "Scenery.h"
#include "ACameraController.h"
#include "CameraControllerCombination.h"

#include <Ogre.h>
#include <OgreShaderGenerator.h>

using namespace Ogre;

Scenery::Scenery(Ogre::Root *root) :
    m_sceneManager(root->createSceneManager()),
    m_linePointer(m_sceneManager),
    m_viewer(std::make_shared<CameraControllerCombination>(m_sceneManager, ARCBALL))
{
}

SceneManager *Scenery::sceneManager()
{
    return m_sceneManager;
}

ACameraController::Ptr & Scenery::viewer()
{
    return m_viewer;
}

void Scenery::setup()
{
    RTShader::ShaderGenerator* shaderGen = RTShader::ShaderGenerator::getSingletonPtr();
    shaderGen->addSceneManager(m_sceneManager);

    m_viewer->SetViewParameters({0, 0, 15}, {0, 0, 0}, Vector3::UNIT_Y);

    Light* light = m_sceneManager->createLight("MainLight");
    SceneNode* lightNode = m_sceneManager->getRootSceneNode()->createChildSceneNode();
    lightNode->setPosition(0, 10, 15);
    lightNode->attachObject(light);

    LinePath3D line {m_sceneManager};
    line.Recreate({
        {-15, -15, -15},
        {15, 15, 15}});
}
