//
// Created by anton on 14.06.22.
//

#include "Visualizer.h"
#include "ois/OIS.h"
#include "LinePath3D.h"
#include "ACameraController.h"

using namespace Ogre;

Visualizer::Visualizer() : OgreBites::ApplicationContext("Visualizer")
{
}

void Visualizer::setup() {
    // do not forget to call the base first
    OgreBites::ApplicationContext::setup();
    ResourceGroupManager::getSingleton().addResourceLocation(
            "/usr/local/share/OGRE/Media/models/", "FileSystem");
    ResourceGroupManager::getSingleton().addResourceLocation(
            "/usr/local/share/OGRE/Media/materials/scripts", "FileSystem");
    ResourceGroupManager::getSingleton().addResourceLocation(
            "/usr/local/share/OGRE/Media/materials/textures", "FileSystem");

    m_scenery = new Scenery(getRoot());
    m_scenery->setup();

    m_scenery->viewer()->SetWindowSize({static_cast<float>(getRenderWindow()->getWidth()),
                                        static_cast<float>(getRenderWindow()->getHeight())});

    // and tell it to render into the main window
    getRenderWindow()->addViewport(m_scenery->viewer()->GetCamera());

    m_inputManager = InputManager::getSingletonPtr();
    m_inputManager->initialise(getRenderWindow());
    m_inputManager->addKeyListener(m_scenery->viewer().get(), "viewer");
    m_inputManager->addMouseListener(m_scenery->viewer().get(), "viewer");
    m_inputManager->addKeyListener(this, "exit");
}

bool Visualizer::frameStarted(const FrameEvent &evt)
{
    m_inputManager->capture();
    return true;
}

bool Visualizer::keyPressed(const OIS::KeyEvent &arg)
{
    if (arg.key == OIS::KC_ESCAPE)
    {
        OgreBites::ApplicationContextBase::getRoot()->queueEndRendering();
    }
    return true;
}

bool Visualizer::keyReleased(const OIS::KeyEvent &arg)
{
    return true;
}

void Visualizer::windowResized(Ogre::RenderWindow *rw)
{
    if (m_scenery) {
        m_scenery->viewer()->SetWindowSize({static_cast<float>(getRenderWindow()->getWidth()),
                                            static_cast<float>(getRenderWindow()->getHeight())});
    }
}
