//
// Created by anton on 20.06.22.
//

#pragma once

#include "ACameraController.h"


class ArrowCameraController : public ACameraController
{
public:
    explicit ArrowCameraController(Ogre::SceneManager *scene) : ACameraController(scene) {}
    ArrowCameraController(Ogre::Camera *camera, Ogre::SceneNode *node) : ACameraController(camera, node) {}

    bool keyPressed(const OIS::KeyEvent &arg) override;
    bool keyReleased(const OIS::KeyEvent &arg) override;

    bool mouseMoved(const OIS::MouseEvent &arg) override;
    bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id) override;
    bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id) override;

    void SetWindowSize(const Ogre::Vector2 &windowSize) override;
    void SetViewParameters(const Ogre::Vector3 &position, const Ogre::Vector3 &targetPoint,
                           const Ogre::Vector3 &up) override;
};
