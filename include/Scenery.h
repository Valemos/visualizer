#pragma once

#include "Ogre.h"

#include "LinePath3D.h"
#include "ArcballCameraController.h"
#include "ACameraController.h"

class Scenery
{
public:
    explicit Scenery(Ogre::Root *root);

    Ogre::SceneManager *sceneManager();
    ACameraController::Ptr & viewer();

    void setup();

private:
    Ogre::SceneManager *m_sceneManager;
    LinePath3D m_linePointer;
    ACameraController::Ptr m_viewer;
};
