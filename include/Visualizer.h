//
// Created by anton on 14.06.22.
//

#pragma once

#include "Ogre.h"
#include "OgreApplicationContext.h"

#include "ois/OIS.h"
#include "ois/OISInputManager.h"

#include "ArcballCameraController.h"
#include "InputManager.h"
#include "Scenery.h"


class Visualizer : public OgreBites::ApplicationContext, OIS::KeyListener {
public:
    Visualizer();
    void setup() override;

    bool frameStarted(const Ogre::FrameEvent &evt) override;
    void windowResized(Ogre::RenderWindow *rw) override;

    bool keyPressed(const OIS::KeyEvent &arg) override;
    bool keyReleased(const OIS::KeyEvent &arg) override;

private:
    InputManager* m_inputManager{};
    Scenery* m_scenery{};
};
