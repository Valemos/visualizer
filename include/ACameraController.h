#pragma once

#include <memory>
#include "ArcballTransformation.h"
#include "Ogre.h"
#include "ois/OIS.h"

class ACameraController : public OIS::KeyListener, public OIS::MouseListener
{
public:
    typedef std::shared_ptr<ACameraController> Ptr;

    explicit ACameraController(Ogre::SceneManager *scene);
    ACameraController(Ogre::Camera *camera, Ogre::SceneNode *node);

    Ogre::Camera *GetCamera();

    virtual void SetWindowSize(const Ogre::Vector2 &windowSize) = 0;
    virtual void SetViewParameters(const Ogre::Vector3 &position,
                                   const Ogre::Vector3 &targetPoint,
                                   const Ogre::Vector3 &up) = 0;

protected:
    Ogre::SceneNode *m_viewerNode;
    Ogre::Camera *m_camera;
};
