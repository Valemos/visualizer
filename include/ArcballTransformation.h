#pragma once

#include "Ogre.h"

#include "OgreQuaternion.h"
#include "OgreDualQuaternion.h"

class ArcballTransformation {
public:
    ArcballTransformation(const Ogre::Vector3& cameraPosition, const Ogre::Vector3& viewCenter,
                          const Ogre::Vector3& upDir, const Ogre::Degree &fov,
                          const Ogre::Vector2& windowSize);

    /* Set the camera view parameters: eye position, view center, up
       direction */
    void SetViewParameters(const Ogre::Vector3& eye, const Ogre::Vector3& viewCenter,
                           const Ogre::Vector3& upDir);

    /* Update screen size after the window has been resized */
    void Reshape(const Ogre::Vector2& windowSize) { m_windowSize = windowSize; }

    /* Initialize the first (screen) mouse position for camera
       transformation. This should be called in mouse pressed event. */
    void InitTransformation(const Ogre::Vector2& mousePos);

    /* Rotate the camera from the previous (screen) mouse position to the
       current (screen) position */
    void Rotate(const Ogre::Vector2& mousePos);

    /* Translate the camera from the previous (screen) mouse position to
       the current (screen) mouse position */
    void Translate(const Ogre::Vector2 &mousePos, float translateFraction);

    /* Translate the camera by the delta amount of (NDC) mouse position.
       Note that NDC position must be in [-1, -1] to [1, 1]. */
    void TranslateDelta(const Ogre::Vector2& translationNDC, float fraction);

    /* Zoom the camera (positive delta = zoom in, negative = Zoom out) */
    void Zoom(float delta);

    /* Field of view */
    Ogre::Degree fov() const { return m_fov; }

    const Ogre::Affine3& view() const { return m_view; }

    const Ogre::Affine3& transformation() const { return m_inverseView; }

    float viewDistance() const { return std::abs(m_zooming); }

    void UpdateViewTransform();

protected:
    Ogre::Degree m_fov;
    Ogre::Vector2 m_windowSize;

    Ogre::Vector3 m_targetPosition;
    Ogre::Vector2 m_prevMousePosNDC;
    Ogre::Quaternion m_targetRotation;
    Ogre::Quaternion m_prevRotation;

    Ogre::Affine3 m_view;
    Ogre::Affine3 m_inverseView;
    float m_zooming{};

    /* Transform from screen coordinate to NDC - normalized device
       coordinate. The top-left of the screen corresponds to [-1, 1] NDC,
       and the bottom right is [1, -1] NDC. */
    Ogre::Vector2 screenCoordToNDC(const Ogre::Vector2& mousePos) const;

};
