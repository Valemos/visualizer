//
// Created by anton on 19.06.22.
//

#pragma once

#include "Ogre.h"

#include <vector>

class LinePath3D
{
public:
    explicit LinePath3D(Ogre::SceneManager* sceneManager);
    void Recreate(const std::vector<Ogre::Vector3> &points);

private:
    Ogre::ManualObject* m_object;
    Ogre::SceneNode* m_node;
    Ogre::MaterialPtr m_material{};
    std::vector<Ogre::Vector3> m_points{};

    static Ogre::MaterialPtr CreateMaterial();
};
