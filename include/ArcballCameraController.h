//
// Created by anton on 13.06.22.
//

#pragma once

#include "ArcballTransformation.h"

#include "Ogre.h"
#include "ois/OIS.h"
#include "ACameraController.h"

#include <memory>

class ArcballCameraController : public ACameraController
{
public:
    explicit ArcballCameraController(Ogre::SceneManager *scene) : ACameraController(scene) {}
    ArcballCameraController(Ogre::Camera *camera, Ogre::SceneNode *node) : ACameraController(camera, node) {}

    bool keyPressed(const OIS::KeyEvent &evt) override;
    bool keyReleased(const OIS::KeyEvent &arg) override;

    bool mouseMoved(const OIS::MouseEvent &evt) override;
    bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id) override;
    bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id) override;

    void SetWindowSize(const Ogre::Vector2 &windowSize) override;
    void SetViewParameters(const Ogre::Vector3 &position,
                           const Ogre::Vector3 &targetPoint,
                           const Ogre::Vector3 &up) override;

private:
    enum ArcballMode {
        DISABLED,
        ROTATION,
        TRANSLATION
    };

    bool m_shiftPressed {false};
    ArcballMode m_arcballMode {DISABLED};
    ArcballTransformation m_cameraTransform {
            Ogre::Vector3::UNIT_X,
            Ogre::Vector3::UNIT_Y,
            Ogre::Vector3::UNIT_Z,
            Ogre::Degree{135}, {1, 1}};

    void UpdateViewNode();
};
