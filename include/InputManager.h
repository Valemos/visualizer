#ifndef InputManager_H
#define InputManager_H

#include "ois/OISMouse.h"
#include "ois/OISKeyboard.h"
#include "ois/OISJoyStick.h"
#include "ois/OISInputManager.h"

#include <OgreRenderWindow.h>

class InputManager : public OIS::KeyListener, public OIS::MouseListener
{
public:
    ~InputManager() override;

    void initialise(Ogre::RenderWindow *renderWindow);

    void capture();

    void addKeyListener(OIS::KeyListener *keyListener, const std::string &instanceName);
    void addMouseListener(OIS::MouseListener *mouseListener, const std::string &instanceName);

    void removeKeyListener(const std::string &instanceName);
    void removeMouseListener(const std::string &instanceName);
    void removeKeyListener(OIS::KeyListener *keyListener);
    void removeMouseListener(OIS::MouseListener *mouseListener);
    void removeAllListeners();
    void removeAllKeyListeners();
    void removeAllMouseListeners();

    void setWindowExtents(int width, int height);

    OIS::Mouse *getMouse();

    OIS::Keyboard *getKeyboard();

    static InputManager *getSingletonPtr();

private:
    InputManager();

    InputManager(const InputManager &) {}

    InputManager &operator=(const InputManager &);

    bool keyPressed(const OIS::KeyEvent &e) override;
    bool keyReleased(const OIS::KeyEvent &e) override;

    bool mouseMoved(const OIS::MouseEvent &e) override;
    bool mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id) override;
    bool mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id) override;

    OIS::Mouse *m_mouse{};
    OIS::Keyboard *m_keyboard{};
    OIS::InputManager *m_inputSystem{};

    std::map<std::string, OIS::KeyListener *> m_keyListeners;
    std::map<std::string, OIS::MouseListener *> m_mouseListeners;

    std::map<std::string, OIS::KeyListener *>::iterator m_itKeyListener;
    std::map<std::string, OIS::MouseListener *>::iterator m_itMouseListener;

    std::map<std::string, OIS::KeyListener *>::iterator m_itKeyListenerEnd;
    std::map<std::string, OIS::MouseListener *>::iterator m_itMouseListenerEnd;

    static InputManager *m_instanceSingleton;
};

#endif