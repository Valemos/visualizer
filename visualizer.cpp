#include "Visualizer.h"

int main( int argc, char *argv[] )
{
    Visualizer app;
    app.initApp();
    app.getRoot()->startRendering();
    app.closeApp();
}
